package com.breed.domain.mapperTest

import com.breed.data.model.SearchBreedResponse
import com.breed.data.model.SearchBreedResponseItem
import com.breed.domain.data.BreedDomainModel
import com.breed.domain.mapper.SearchBreedResponseToDomainModelMapper
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class SearchBreedResponseToDomainModelMapperTest {
    private lateinit var classUnderTest: SearchBreedResponseToDomainModelMapper

    @Before
    fun setUp() {
        classUnderTest = SearchBreedResponseToDomainModelMapper()
    }

    @Test
    fun `Given SearchBreedResponse when toDomain then return list of BreedDomainModel`() {
        // Given
        val name1 = "Bla"
        val refId1 = "Id1"

        val name2 = "Bla2"
        val refId2 = "Id2"

        val searchBreedResponse = SearchBreedResponse()
        val element1 = SearchBreedResponseItem(name1, refId1)
        val element2 = SearchBreedResponseItem(name2, refId2)
        searchBreedResponse.add(element1)
        searchBreedResponse.add(element2)


        val breedDomainModel1 = BreedDomainModel(name1, refId1)
        val breedDomainModel2 = BreedDomainModel(name2, refId2)
        val expected = listOf(breedDomainModel1, breedDomainModel2)

        // When
        val actualValue = classUnderTest.toDomain(searchBreedResponse)

        // Then
        assertEquals(expected, actualValue)
    }
}