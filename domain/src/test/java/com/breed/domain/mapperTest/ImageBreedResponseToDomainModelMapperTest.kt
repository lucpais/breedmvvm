package com.breed.domain.mapperTest

import com.breed.data.model.ImageBreedResponse
import com.breed.data.model.SearchBreedResponseItem
import com.breed.domain.data.BreedImageDomainModel
import com.breed.domain.mapper.ImageBreedResponseToDomainModelMapper
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ImageBreedResponseToDomainModelMapperTest {
    private lateinit var classUnderTest: ImageBreedResponseToDomainModelMapper

    @Before
    fun setUp() {
        classUnderTest = ImageBreedResponseToDomainModelMapper()
    }

    @Test
    fun `Given ImageBreedResponse when toDomain then return a BreedImageDomainModel`() {
        // Given
        val name1 = "Bla"
        val url = "Url"
        val id = "Id1"

        val listBreed = listOf(SearchBreedResponseItem(name1, id))
        val imageBreedResponse = ImageBreedResponse(listBreed, url)

        val expected = BreedImageDomainModel(name1, url)

        // When
        val actualValue = classUnderTest.toDomain(imageBreedResponse)

        // Then
        assertEquals(expected, actualValue)
    }
}