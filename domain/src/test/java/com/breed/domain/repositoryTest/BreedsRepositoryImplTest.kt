package com.breed.domain.repositoryTest

import com.breed.data.model.ImageBreedResponse
import com.breed.data.model.SearchBreedResponse
import com.breed.data.model.SearchBreedResponseItem
import com.breed.data.source.BreedsRemoteSource
import com.breed.domain.data.BreedDomainModel
import com.breed.domain.data.BreedImageDomainModel
import com.breed.domain.mapper.ImageBreedResponseToDomainModelMapper
import com.breed.domain.mapper.SearchBreedResponseToDomainModelMapper
import com.breed.domain.repository.BreedsRepositoryImpl
import com.nhaarman.mockito_kotlin.given
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BreedsRepositoryImplTest {
    private lateinit var classUnderTest: BreedsRepositoryImpl

    @Mock
    lateinit var remoteSource: BreedsRemoteSource

    @Mock
    lateinit var imageBreedResponseToDomainModelMapper: ImageBreedResponseToDomainModelMapper

    @Mock
    lateinit var searchBreedResponseToDomainModelMapper: SearchBreedResponseToDomainModelMapper

    @Before
    fun setUp() {
        classUnderTest = BreedsRepositoryImpl(
            remoteSource,
            imageBreedResponseToDomainModelMapper,
            searchBreedResponseToDomainModelMapper
        )
    }

    @Test
    fun `When getBreedsForName then return a list of BreedDomainModel`() {
        // Given
        val searchBreedResponse = SearchBreedResponse()
        searchBreedResponse.add(SearchBreedResponseItem("name", "id"))

        val searchBreedDomainModel = BreedDomainModel("name", "id")
        val searchBreedList = listOf(searchBreedDomainModel)

        given { remoteSource.getBreedsByName("searchString") }.willReturn(
            Single.just(searchBreedResponse)
        )
        given { searchBreedResponseToDomainModelMapper.toDomain(searchBreedResponse) }.willReturn(
            searchBreedList
        )

        // When
        val actualValue = classUnderTest.getBreedsForName("searchString")

        // Then
        actualValue
            .test()
            .assertValue(searchBreedList)
            .assertNoErrors()
            .assertComplete()
    }

    @Test
    fun `When imageFromBreedId then return a BreedImageDomainModel`() {
        // Given
        val breedItemList = listOf(SearchBreedResponseItem("name", "id"))
        val searchBreedResponse = ImageBreedResponse(breedItemList, "url")

        val breedImageDomainModel = BreedImageDomainModel("name", "url")

        given { remoteSource.getBreedImage("id") }.willReturn(
            Single.just(searchBreedResponse)
        )
        given { imageBreedResponseToDomainModelMapper.toDomain(searchBreedResponse) }.willReturn(
            breedImageDomainModel
        )

        // When
        val actualValue = classUnderTest.imageFromBreedId("id")

        // Then
        actualValue
            .test()
            .assertValue(breedImageDomainModel)
            .assertNoErrors()
            .assertComplete()
    }

}
