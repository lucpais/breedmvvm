package com.breed.domain.usecaseTest

import com.breed.domain.data.BreedImageDomainModel
import com.breed.domain.repository.BreedsRepositoryImpl
import com.breed.domain.usecase.GetBreedImageUseCase
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetBreedImageUseCaseTest {
    private lateinit var classUnderTest: GetBreedImageUseCase

    @Mock
    lateinit var repository: BreedsRepositoryImpl

    @Before
    fun setUp() {
        classUnderTest = GetBreedImageUseCase(repository)
    }

    @Test
    fun `When execute then returns expected BreedImageDomainModel`() {
        // Given
        val imageBreedDomainModel = BreedImageDomainModel("name", "url")

        whenever(repository.imageFromBreedId("id"))
            .thenReturn(Single.just(imageBreedDomainModel))

        // When
        val actualValue = classUnderTest.execute("id")

        // Then
        actualValue.test()
            .assertResult(imageBreedDomainModel)
            .assertComplete()
            .assertNoErrors()
    }
}
