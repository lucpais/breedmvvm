package com.breed.domain.usecaseTest

import com.breed.domain.data.BreedDomainModel
import com.breed.domain.repository.BreedsRepositoryImpl
import com.breed.domain.usecase.GetBreedsByNameUseCase
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetBreedsByNameUseCaseTest {
    private lateinit var classUnderTest: GetBreedsByNameUseCase

    @Mock
    lateinit var repository: BreedsRepositoryImpl

    @Before
    fun setUp() {
        classUnderTest = GetBreedsByNameUseCase(repository)
    }

    @Test
    fun `When execute then returns expected list of BreedDomainModel`() {
        // Given
        val searchBreedDomainModel = BreedDomainModel("name", "id")
        val searchBreedDomainList = listOf(searchBreedDomainModel)

        whenever(repository.getBreedsForName("name"))
            .thenReturn(Single.just(searchBreedDomainList))

        // When
        val actualValue = classUnderTest.execute("name")

        // Then
        actualValue.test()
            .assertResult(searchBreedDomainList)
            .assertComplete()
            .assertNoErrors()
    }
}
