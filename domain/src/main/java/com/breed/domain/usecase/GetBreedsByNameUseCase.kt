package com.breed.domain.usecase

import com.breed.domain.repository.BreedsRepositoryImpl
import dagger.Reusable
import io.reactivex.Single
import javax.inject.Inject

@Reusable
class GetBreedsByNameUseCase @Inject constructor(
    private val breedsRepository: BreedsRepositoryImpl
) {
    fun execute(breedName: String) =
        breedsRepository.getBreedsForName(breedName).flatMap { listBreedsDomainModel ->
            Single.just(listBreedsDomainModel)
        }
}

