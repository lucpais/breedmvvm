package com.breed.domain.usecase

import com.breed.domain.repository.BreedsRepositoryImpl
import dagger.Reusable
import io.reactivex.Single
import javax.inject.Inject

@Reusable
class GetBreedImageUseCase @Inject constructor(
    private val breedsRepository: BreedsRepositoryImpl
) {
    fun execute(breedId: String) =
        breedsRepository.imageFromBreedId(breedId).flatMap { breedImageDomainModel ->
            Single.just(breedImageDomainModel)
        }
}

