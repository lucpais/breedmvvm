package com.breed.domain.data

data class BreedDomainModel(
    val name: String,
    val referenceImageId: String
)