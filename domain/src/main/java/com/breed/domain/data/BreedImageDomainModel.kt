package com.breed.domain.data

data class BreedImageDomainModel(
    val name: String,
    val url: String
)