package com.breed.domain.di

import com.breed.data.source.BreedsRemoteSource
import com.breed.domain.mapper.ImageBreedResponseToDomainModelMapper
import com.breed.domain.mapper.SearchBreedResponseToDomainModelMapper
import com.breed.domain.repository.BreedsRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Module
    companion object {
        @Provides
        fun provideBreedRepository(
            remoteSource: BreedsRemoteSource,
            imageBreedResponseToDomainModelMapper: ImageBreedResponseToDomainModelMapper,
            searchBreedResponseToDomainModelMapper: SearchBreedResponseToDomainModelMapper
        ) = BreedsRepositoryImpl(
            remoteSource,
            imageBreedResponseToDomainModelMapper,
            searchBreedResponseToDomainModelMapper
        )
    }
}