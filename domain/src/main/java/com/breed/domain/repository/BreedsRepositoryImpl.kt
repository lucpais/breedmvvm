package com.breed.domain.repository

import com.breed.data.source.BreedsRemoteSource
import com.breed.domain.data.BreedDomainModel
import com.breed.domain.data.BreedImageDomainModel
import com.breed.domain.mapper.ImageBreedResponseToDomainModelMapper
import com.breed.domain.mapper.SearchBreedResponseToDomainModelMapper
import io.reactivex.Single
import javax.inject.Inject

interface BreedsRepository {
    fun imageFromBreedId(breedId: String): Single<BreedImageDomainModel>

    fun getBreedsForName(breedName: String): Single<List<BreedDomainModel>>
}

class BreedsRepositoryImpl @Inject constructor(
    private val remoteSource: BreedsRemoteSource,
    private val imageBreedResponseToDomainModelMapper: ImageBreedResponseToDomainModelMapper,
    private val searchBreedResponseToDomainModelMapper: SearchBreedResponseToDomainModelMapper
) : BreedsRepository {

    override fun imageFromBreedId(breedId: String): Single<BreedImageDomainModel> =
        remoteSource.getBreedImage(breedId).map { imageResponse ->
            imageBreedResponseToDomainModelMapper.toDomain(imageResponse)
        }

    override fun getBreedsForName(breedName: String): Single<List<BreedDomainModel>> =
        remoteSource.getBreedsByName(breedName).map { searchBreedResponse ->
            searchBreedResponseToDomainModelMapper.toDomain(searchBreedResponse)
        }
}