package com.breed.domain.mapper

import com.breed.data.model.ImageBreedResponse
import com.breed.domain.data.BreedImageDomainModel
import dagger.Reusable
import javax.inject.Inject

@Reusable
class ImageBreedResponseToDomainModelMapper @Inject constructor() {
    fun toDomain(imageBreedResponse: ImageBreedResponse) =
        BreedImageDomainModel(
            name = imageBreedResponse.breeds.first().name,
            url = imageBreedResponse.url
        )
}