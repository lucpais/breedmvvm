package com.breed.domain.mapper

import com.breed.data.model.SearchBreedResponse
import com.breed.domain.data.BreedDomainModel
import dagger.Reusable
import javax.inject.Inject

@Reusable
class SearchBreedResponseToDomainModelMapper @Inject constructor() {
    fun toDomain(searchBreedResponse: SearchBreedResponse) =
        searchBreedResponse.filter { !it.name.isNullOrEmpty() && !it.referenceImageId.isNullOrEmpty() }
            .map { breedResponse ->
                BreedDomainModel(
                    name = breedResponse.name,
                    referenceImageId = breedResponse.referenceImageId
                )
            }
}