package com.breed.core.presentation

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface SchedulerProvider {

    fun io(): Scheduler
    fun ui(): Scheduler

    fun <T> doOnIoObserveOnMainSingle(): SingleTransformer<T, T>
}

abstract class BaseSchedulerProvider :
    SchedulerProvider {

    override fun <T> doOnIoObserveOnMainSingle(): SingleTransformer<T, T> {
        return SingleTransformer {
            it.subscribeOn(io())
                .unsubscribeOn(io())
                .observeOn(ui())
        }
    }
}

class AppSchedulerProvider : BaseSchedulerProvider() {
    override fun io(): Scheduler = Schedulers.io()
    override fun ui(): Scheduler = AndroidSchedulers.mainThread()
}
