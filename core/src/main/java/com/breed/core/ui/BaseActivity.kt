package com.breed.core.ui

import androidx.lifecycle.ViewModel
import com.breed.core.presentation.SchedulerProvider
import com.breed.core.presentation.ViewModelProviderFactory
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseActivity<T : ViewModel> : DaggerAppCompatActivity(),
    BaseView<T> {

    @Inject
    protected lateinit var vmFactory: ViewModelProviderFactory<T>

    @Inject
    protected lateinit var schedulers: SchedulerProvider

    protected val subscriptions = CompositeDisposable()

    override fun onDestroy() {
        subscriptions.clear()
        super.onDestroy()
    }
}
