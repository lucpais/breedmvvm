package com.breed.data.source

import com.breed.data.ApiService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BreedsRemoteSource
@Inject constructor(
    private val apiService: ApiService
) {
    fun getBreedsByName(breedName: String) =
        apiService.getBreedsByName(breedName)

    fun getBreedImage(breedId: String) = apiService.getBreedImage(breedId)
}
