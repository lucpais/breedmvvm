package com.breed.data

import com.breed.data.model.ImageBreedResponse
import com.breed.data.model.SearchBreedResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("breeds/search?")
    fun getBreedsByName(
        @Query("q") breedName: String
    ): Single<SearchBreedResponse>

    @GET("images/{image_id}")
    fun getBreedImage(
        @Path("image_id") breedId: String
    ): Single<ImageBreedResponse>

}
