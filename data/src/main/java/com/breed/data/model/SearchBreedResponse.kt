package com.breed.data.model

import com.google.gson.annotations.SerializedName


class SearchBreedResponse : ArrayList<SearchBreedResponseItem>()

data class SearchBreedResponseItem(
    val name: String,
    @SerializedName("reference_image_id")
    val referenceImageId: String
)