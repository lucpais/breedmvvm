package com.breed.data.model


data class ImageBreedResponse(
    val breeds: List<SearchBreedResponseItem>,
    val url: String
)