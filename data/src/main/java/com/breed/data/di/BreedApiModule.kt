package com.breed.data.di

import com.breed.data.ApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class BreedApiModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        @Singleton
        internal fun provideBreedApi(retrofit: Retrofit) =
            retrofit.create(ApiService::class.java)

        @Provides
        @JvmStatic
        @Singleton
        internal fun provideBreedRetrofit(
            httpBuilder: OkHttpClient.Builder,
            retrofitBuilder: Retrofit.Builder
        ) = retrofitBuilder
            .client(httpBuilder.build())
            .build()
    }
}