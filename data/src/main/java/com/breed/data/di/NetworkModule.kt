package com.breed.data.di

import android.content.Context
import android.net.ConnectivityManager
import com.breed.data.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule(
        private val baseUrlOverride: String? = null,
        private val apiKey: String? = null
) {
    @Provides
    @BaseUrl
    fun provideBaseUrl() = baseUrlOverride ?: "https://api.thedogapi.com/v1/"

    @Provides
    @ApiKey
    fun provideApiKey() = apiKey ?: "7942232b-b4e4-4d2d-921e-a8f23fce0922"

    @Provides
    @Singleton
    fun provideRetrofitBuilder(
            rxJavaCallAdapterFactory: RxJava2CallAdapterFactory,
            gsonConverterFactory: GsonConverterFactory,
            @BaseUrl baseUrl: String
    ) = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(rxJavaCallAdapterFactory)

    @Provides
    @Singleton
    fun provideHttpBuilder(@ApiKey apiKey: String) =
            OkHttpClient.Builder().apply {
                if (BuildConfig.DEBUG) {
                    val httpLoggingInterceptor = HttpLoggingInterceptor()
                    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                    addInterceptor(httpLoggingInterceptor)
                }
                addInterceptor { chain ->
                    val original = chain.request()

                    // Request customization: add request headers
                    val requestBuilder = original.newBuilder()
                            .header("x-api-key", apiKey)

                    val request = requestBuilder.build()
                    chain.proceed(request)
                }

                readTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                connectTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
            }


    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideGsonConverterFactory(
            gson: Gson
    ): GsonConverterFactory = GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    fun provideRxJavaCallAdapter(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    @Provides
    @Singleton
    fun provideConnectivityManager(context: Context): ConnectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    companion object {
        const val RETROFIT_TIMEOUT = 60L
    }
}