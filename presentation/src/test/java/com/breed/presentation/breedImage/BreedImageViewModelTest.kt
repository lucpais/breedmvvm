package com.breed.presentation.breedImage

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.breed.domain.data.BreedImageDomainModel
import com.breed.domain.usecase.GetBreedImageUseCase
import com.breed.presentation.mapper.BreedImageDomainModelToUiModelMapper
import com.breed.presentation.model.BreedImageUIModel
import com.breed.presentation.setupViewModelForTests
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BreedImageViewModelTest {
    private lateinit var classUnderTest: BreedImageViewModel

    @Mock
    lateinit var getBreedImageUseCase: GetBreedImageUseCase

    @Mock
    lateinit var breedImageDomainModelToUiModelMapper: BreedImageDomainModelToUiModelMapper

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var breedImageMutableDataTestObserver: Observer<BreedImageUIModel>

    @Before
    fun setUp() {
        classUnderTest = BreedImageViewModel(
            getBreedImageUseCase,
            breedImageDomainModelToUiModelMapper
        )
        setupViewModelForTests(classUnderTest)

        breedImageMutableDataTestObserver = mock()

        classUnderTest.breedImageObservable().observeForever(breedImageMutableDataTestObserver)

    }

    @Test
    fun `When getBreedsByName then searchBreedListRetrievedTestObserver invoked`() {
        // Given
        val name = "Bla"
        val url = "url"

        val breedImageDomainModel = BreedImageDomainModel(name, url)
        val breddImageUIModel = BreedImageUIModel(name, url)

        whenever(breedImageDomainModelToUiModelMapper.toUi(breedImageDomainModel)).thenReturn(
            breddImageUIModel
        )
        whenever(getBreedImageUseCase.execute("id")).thenReturn(
            Single.just(breedImageDomainModel)
        )
        // When
        classUnderTest.getBreedsImage("id")

        // Then
        val captor = ArgumentCaptor.forClass(BreedImageUIModel::class.java)
        captor.run {
            verify(breedImageMutableDataTestObserver, times(1)).onChanged(capture())
            assertEquals(breddImageUIModel, value)
        }
    }
}

