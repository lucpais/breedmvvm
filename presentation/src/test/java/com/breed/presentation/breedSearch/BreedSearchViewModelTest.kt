package com.breed.presentation.breedSearch

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.breed.domain.data.BreedDomainModel
import com.breed.domain.usecase.GetBreedsByNameUseCase
import com.breed.presentation.mapper.BreedDomainModelToUiModelMapper
import com.breed.presentation.model.BreedUIModel
import com.breed.presentation.setupViewModelForTests
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BreedSearchViewModelTest {
    private lateinit var classUnderTest: BreedSearchViewModel

    @Mock
    lateinit var getBreedsByNameUseCase: GetBreedsByNameUseCase

    @Mock
    lateinit var breedDomainModelToUiModelMapper: BreedDomainModelToUiModelMapper

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var searchBreedListRetrievedTestObserver: Observer<List<BreedUIModel>>
    private lateinit var breedSelectedTestObserver: Observer<String>

    @Before
    fun setUp() {
        classUnderTest = BreedSearchViewModel(
            getBreedsByNameUseCase,
            breedDomainModelToUiModelMapper
        )
        setupViewModelForTests(classUnderTest)

        searchBreedListRetrievedTestObserver = mock()
        breedSelectedTestObserver = mock()

        classUnderTest.searchBreedListObservable().observeForever(searchBreedListRetrievedTestObserver)
        classUnderTest.breedSelectedObservable().observeForever(breedSelectedTestObserver)
    }

    @Test
    fun `When getBreedsByName then searchBreedListRetrievedTestObserver invoked`() {
        // Given
        val name1 = "Bla"
        val refId1 = "Id1"

        val breedDomainModel1 = BreedDomainModel(name1, refId1)

        val breedUiModel1 = BreedUIModel(name1, refId1)
        val lisOfBreedUiModels = listOf(breedUiModel1)

        whenever(breedDomainModelToUiModelMapper.toUi(breedDomainModel1)).thenReturn(
            breedUiModel1
        )
        whenever(getBreedsByNameUseCase.execute("name")).thenReturn(
            Single.just(listOf(breedDomainModel1))
        )
        // When
        classUnderTest.getBreedsByName("name")

        // Then
        val captor = ArgumentCaptor.forClass(BreedUIModel::class.java)
        captor.run {
            verify(searchBreedListRetrievedTestObserver, times(1)).onChanged(listOf(capture()))
            assertEquals(lisOfBreedUiModels, value)
        }
    }

    @Test
    fun `When onBreedSelected then breedSelectedTestObserver invoked`() {
        // Given
        val name = "name"

        // When
        classUnderTest.onBreedSelected("name")

        // Then
        val captor = ArgumentCaptor.forClass(String::class.java)
        captor.run {
            verify(breedSelectedTestObserver, times(1)).onChanged(capture())
            assertEquals(name, value)
        }
    }

}

