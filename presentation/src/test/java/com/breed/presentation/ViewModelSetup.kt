package com.breed.presentation

import com.breed.core.presentation.BaseViewModel
import com.breed.presentation.scheduler.TestSchedulerProvider


fun setupViewModelForTests(baseViewModel: BaseViewModel) {
    baseViewModel.schedulerProvider = TestSchedulerProvider()
}
