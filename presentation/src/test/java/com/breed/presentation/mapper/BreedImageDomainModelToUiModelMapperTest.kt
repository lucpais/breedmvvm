package com.breed.presentation.mapper

import com.breed.domain.data.BreedImageDomainModel
import com.breed.presentation.model.BreedImageUIModel
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class BreedImageDomainModelToUiModelMapperTest {
    private lateinit var classUnderTest: BreedImageDomainModelToUiModelMapper

    @Before
    fun setUp() {
        classUnderTest = BreedImageDomainModelToUiModelMapper()
    }

    @Test
    fun `Given BreedImageDomainModel when toUI then return BreedImageUIModel`() {
        //Given
        val name = "name"
        val url = "url"

        val breedImageDomainModel = BreedImageDomainModel(name, url)

        val expected = BreedImageUIModel(name, url)

        // When
        val actualValue = classUnderTest.toUi(breedImageDomainModel)

        //Then
        assertEquals(expected, actualValue)

    }

}