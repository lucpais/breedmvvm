package com.breed.presentation.mapper

import com.breed.domain.data.BreedDomainModel
import com.breed.presentation.model.BreedUIModel
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class BreedDomainModelToUiModelMapperTest {
    private lateinit var classUnderTest: BreedDomainModelToUiModelMapper

    @Before
    fun setUp() {
        classUnderTest = BreedDomainModelToUiModelMapper()
    }

    @Test
    fun `Given BreedDomainModel when toUI then return BreedUIModel`() {
        //Given
        val name = "name"
        val id = "id"

        val breedDomainModel = BreedDomainModel(name, id)

        val expected = BreedUIModel(name, id)

        // When
        val actualValue = classUnderTest.toUi(breedDomainModel)

        //Then
        assertEquals(expected, actualValue)

    }

}