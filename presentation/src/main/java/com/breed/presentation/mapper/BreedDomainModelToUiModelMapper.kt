package com.breed.presentation.mapper

import com.breed.domain.data.BreedDomainModel
import com.breed.presentation.model.BreedUIModel
import javax.inject.Inject

class BreedDomainModelToUiModelMapper @Inject constructor() {
    fun toUi(breedDomainModel: BreedDomainModel) =
        BreedUIModel(
            name = breedDomainModel.name,
            referenceImageId = breedDomainModel.referenceImageId
        )
}