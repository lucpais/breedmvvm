package com.breed.presentation.mapper

import com.breed.domain.data.BreedImageDomainModel
import com.breed.presentation.model.BreedImageUIModel
import javax.inject.Inject

class BreedImageDomainModelToUiModelMapper @Inject constructor() {
    fun toUi(breedImageDomainModel: BreedImageDomainModel) =
        BreedImageUIModel(
            name = breedImageDomainModel.name,
            url = breedImageDomainModel.url
        )
}