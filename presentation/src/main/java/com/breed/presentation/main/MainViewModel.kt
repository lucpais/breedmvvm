package com.breed.presentation.main

import com.breed.core.presentation.BaseViewModel
import com.breed.core.presentation.BaseViewModelInputs
import com.breed.core.presentation.BaseViewModelOutputs
import javax.inject.Inject

interface MainViewModelInputs : BaseViewModelInputs

interface MainViewModelOutputs : BaseViewModelOutputs

open class MainViewModel @Inject constructor() : BaseViewModel(),
    MainViewModelInputs,
    MainViewModelOutputs {

    override val inputs: MainViewModelInputs
        get() = this

    override val outputs: MainViewModelOutputs
        get() = this
}