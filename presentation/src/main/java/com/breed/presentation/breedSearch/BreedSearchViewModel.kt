package com.breed.presentation.breedSearch

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breed.core.presentation.BaseViewModel
import com.breed.core.presentation.BaseViewModelInputs
import com.breed.domain.usecase.GetBreedsByNameUseCase
import com.breed.presentation.R
import com.breed.presentation.mapper.BreedDomainModelToUiModelMapper
import com.breed.presentation.model.BreedUIModel
import timber.log.Timber
import javax.inject.Inject

interface BreedListViewModelInputs : BaseViewModelInputs {
    fun getBreedsByName(breedName: String)
    fun onBreedSelected(breedId: String)
}

class BreedSearchViewModel @Inject constructor(
    private val getBreedsByNameUseCase: GetBreedsByNameUseCase,
    private val breedDomainModelToUiModelMapper: BreedDomainModelToUiModelMapper
) : BaseViewModel(), BreedListViewModelInputs {

    private val searchBreedListRetrieved: MutableLiveData<List<BreedUIModel>> = MutableLiveData()
    private val breedSelected: MutableLiveData<String> = MutableLiveData()

    private val loading: MutableLiveData<Boolean> = MutableLiveData()
    fun loading(): LiveData<Boolean> = loading


    fun searchBreedListObservable(): LiveData<List<BreedUIModel>> = searchBreedListRetrieved

    fun breedSelectedObservable(): LiveData<String> = breedSelected


    override val inputs: BreedListViewModelInputs
        get() = this

    override fun getBreedsByName(breedName: String) {
        getBreedsByNameUseCase.execute(breedName)
            .compose(schedulerProvider.doOnIoObserveOnMainSingle())
            .doOnSubscribe { loading.postValue(true) }
            .doFinally { loading.postValue(false) }
            .subscribe({ breedDomainModelList ->
                searchBreedListRetrieved.postValue(
                    breedDomainModelList.map {
                        breedDomainModelToUiModelMapper.toUi(it)
                    })
            }, { throwable ->
                Timber.d(throwable)
                errorResource.value = R.string.generic_error_retrieving_breeds
            }).also { disposable ->
                subscriptions.add(disposable)
            }
    }

    override fun onBreedSelected(breedId: String) {
        breedSelected.postValue(breedId)
    }

}

