package com.breed.presentation.model

data class BreedImageUIModel(
    val name: String,
    val url: String
)