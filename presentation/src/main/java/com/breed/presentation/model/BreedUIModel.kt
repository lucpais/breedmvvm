package com.breed.presentation.model

data class BreedUIModel(
    val name: String,
    val referenceImageId: String
)