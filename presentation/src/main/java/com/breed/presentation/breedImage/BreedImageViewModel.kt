package com.breed.presentation.breedImage

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breed.core.presentation.BaseViewModel
import com.breed.core.presentation.BaseViewModelInputs
import com.breed.domain.usecase.GetBreedImageUseCase
import com.breed.presentation.R
import com.breed.presentation.mapper.BreedImageDomainModelToUiModelMapper
import com.breed.presentation.model.BreedImageUIModel
import timber.log.Timber
import javax.inject.Inject

interface BreedImageInputs : BaseViewModelInputs {
    fun getBreedsImage(breedId: String)
}

class BreedImageViewModel @Inject constructor(
    private val getBreedImageUseCase: GetBreedImageUseCase,
    private val breedImageDomainModelToUiModelMapper: BreedImageDomainModelToUiModelMapper
) : BaseViewModel(), BreedImageInputs {

    private val breedImageMutableData: MutableLiveData<BreedImageUIModel> = MutableLiveData()

    private val loading: MutableLiveData<Boolean> = MutableLiveData()
    fun loading(): LiveData<Boolean> = loading


    fun breedImageObservable(): LiveData<BreedImageUIModel> = breedImageMutableData

    override val inputs: BreedImageInputs
        get() = this

    override fun getBreedsImage(breedId: String) {
        getBreedImageUseCase.execute(breedId)
            .compose(schedulerProvider.doOnIoObserveOnMainSingle())
            .doOnSubscribe { loading.postValue(true) }
            .doFinally { loading.postValue(false) }
            .subscribe({ breedImageDomainModel ->
                breedImageMutableData.postValue(
                    breedImageDomainModelToUiModelMapper.toUi(breedImageDomainModel)
                )
            }, { throwable ->
                Timber.d(throwable)
                errorResource.value = R.string.generic_error_retrieving_image
            }).also { disposable ->
                subscriptions.add(disposable)
            }
    }
}

