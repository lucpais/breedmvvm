# BreedMVVM: A multi-modular approach of MVVM code challenge

The purpose of this project, is to create a MVVM Android code-challenge, in a multi-module format that helps accomplish the following requirements:

1. Native application for Android.
2. Consume API, https://docs.thedogapi.com/.
    - GET **breeds/search?q={breed_name}**
        > This will be used to retrieve a list of breeds that match the search criteria
    - GET **/images/{image_id}**
        > This will be used to retrieve a url to an image for a selected breed. **image_id** will be retrieved in previous step as **reference_image_id**
3. First Screen will present a Search Box where we type a search criteria. If the search criteria is not null nor empty, search button will be enabled.
4. By pressing search button, a list of breeds that respond to that criteria will be retrieved and showed in the screen.
5. By touching one of the rows, another screen is open with a picture for that particular breed.

---
## Module overview

### Base Android Library
>**base-android-library.gradle** provide plugins and base dependencies for all modules

### Dependencies Gradle
>**depencies.gradle** in **buildsystem** folder is the central dependencies file, where we state all versions for dependencies accross the project

### Core Module
>This module will be use to inject core elements to the app and presentation modules. Contains no tests-

### App module
>This is the UI module, Contails the fragments and activities to be used.
Also, has Espresso instrumentation test for the UI

### Presentatio module
>This is the ViewModel module, all ViewModels for the different (only two so far...) features. ViewModels are unit-tested using Mockito

### Data module
>This is the api engaging module, and will retrieve the information from API, and return it to **Domain module**.  Contains no tests-

### Domain module
>This is the business logic module, and contains all UseCases for different interactions on the app, as to the mappers used to transform Data-specific responses into Domain models. UseCases and Mappers are unit-tested using Mockito.