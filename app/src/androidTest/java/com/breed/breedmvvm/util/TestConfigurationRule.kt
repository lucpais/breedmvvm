package com.breed.breedmvvm.util

import androidx.test.core.app.launchActivity
import com.breed.breedmvvm.view.MainActivity
import com.breed.breedmvvm.webmock.injectTestConfiguration
import org.junit.rules.TestWatcher
import org.junit.runner.Description

class TestConfigurationRule : TestWatcher() {
    override fun starting(description: Description?) {
        super.starting(description)

        injectTestConfiguration {}
        launchActivity<MainActivity>()
    }
}