package com.breed.breedmvvm.breedSearch

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.breed.breedmvvm.R
import org.hamcrest.Matchers

fun breedSearchFragmentRobot(func: BreedSearchFragmentRobot.() -> Unit) = BreedSearchFragmentRobot()
    .apply { func() }

class BreedSearchFragmentRobot {

    fun clickSearchEditTextView() = apply {
        Espresso.onView(searchEditTextViewMatcher).perform(ViewActions.click())
    }

    fun inputSearchEditTextView(text: String) = apply {
        Espresso.onView(searchEditTextViewMatcher).perform(ViewActions.typeText(text))
    }

    fun assertSearchEditTextViewDisplayed() = apply {
        Espresso.onView(searchEditTextViewMatcher)
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    fun assertSearchButtonViewDisplayed() = apply {
        Espresso.onView(searchButtonMatcher)
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    fun assertAnatolianShepherdIsDisplayed() = apply {
        Espresso.onView(anatolianShepherdDog)
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    fun clickSearchButton() = apply {
        Espresso.onView(searchButtonMatcher).perform(ViewActions.click())
    }

    fun assertSearchButtonIsEnabled() = apply {
        Espresso.onView(searchButtonMatcher).check(ViewAssertions.matches(ViewMatchers.isEnabled()))
    }

    fun assertSearchButtonIsDisbled() = apply {
        Espresso.onView(searchButtonMatcher)
            .check(ViewAssertions.matches(Matchers.not(ViewMatchers.isEnabled())))
    }

    companion object {
        private val searchEditTextViewMatcher = withId(R.id.searchEditText)
        private val searchButtonMatcher = withId(R.id.buttonSearchBreed)
        private val anatolianShepherdDog = withText("Anatolian Shepherd Dog")
    }
}