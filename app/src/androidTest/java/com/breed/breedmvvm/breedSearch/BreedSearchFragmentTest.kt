package com.breed.breedmvvm.breedSearch

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.breed.breedmvvm.BuildConfig
import com.breed.breedmvvm.util.TestConfigurationRule
import com.breed.breedmvvm.webmock.SuccessDispatcher
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BreedSearchFragmentTest {

    @get:Rule
    val espressoRule = TestConfigurationRule()

    private val mockWebServer = MockWebServer()

    @Before
    fun setup() {
        mockWebServer.start(BuildConfig.PORT)
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }

    @Test
    fun testElementsOnScreenAreDisplayed() {
        breedSearchFragmentRobot {
            assertSearchEditTextViewDisplayed()
            assertSearchButtonViewDisplayed()
        }
    }

    @Test
    fun testButtonIsEnabledWhenThereIsTextOnEditText() {
        breedSearchFragmentRobot {
            assertSearchButtonIsDisbled()
            clickSearchEditTextView()
            inputSearchEditTextView("Shepherd")
            assertSearchButtonIsEnabled()
        }
    }

    @Test
    fun testSearchShepherdDogsDisplaysShepherdDogs() {
        mockWebServer.dispatcher = SuccessDispatcher()

        breedSearchFragmentRobot {
            clickSearchEditTextView()
            inputSearchEditTextView("Shepherd")
            clickSearchButton()
            assertAnatolianShepherdIsDisplayed()
        }

    }
}