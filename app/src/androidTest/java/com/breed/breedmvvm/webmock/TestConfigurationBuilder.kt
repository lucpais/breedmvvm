package com.breed.breedmvvm.webmock

import androidx.test.platform.app.InstrumentationRegistry
import com.breed.breedmvvm.BreedsApp
import com.breed.breedmvvm.BuildConfig
import com.breed.breedmvvm.di.AppComponent
import com.breed.breedmvvm.di.DaggerAppComponent
import com.breed.data.di.NetworkModule

class TestConfigurationBuilder {
    private val baseUrl: String = "http://127.0.0.1:${BuildConfig.PORT}"
    private val apiKey: String = "7942232b-b4e4-4d2d-921e-a8f23fce0922"

    fun inject() {
        appComponent {
            networkModule(NetworkModule(baseUrl, apiKey))
        }.inject(requireTestedApplication())
    }
}

fun injectTestConfiguration(block: TestConfigurationBuilder.() -> Unit) {
    TestConfigurationBuilder().apply(block).inject()
}

private fun appComponent(block: DaggerAppComponent.Builder.() -> Unit = {}): AppComponent =
    DaggerAppComponent.builder().apply(block).build()

private fun requireTestedApplication() =
    (InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as BreedsApp)