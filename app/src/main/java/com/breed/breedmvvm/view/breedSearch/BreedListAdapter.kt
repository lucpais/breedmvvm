package com.breed.breedmvvm.view.breedSearch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.breed.breedmvvm.R
import com.breed.presentation.model.BreedUIModel
import kotlinx.android.synthetic.main.breed_item.view.*

interface BindableAdapter<T> {
    fun setData(data: T)
}

interface ClickListener {
    fun onItemClicked(breedId: String)
}

class BreedListAdapter(private val clickListener: ClickListener) :
    RecyclerView.Adapter<BreedListAdapter.BreedHolder>(),
    BindableAdapter<List<BreedUIModel>> {

    private val breeds = mutableListOf<BreedUIModel>()

    override fun setData(data: List<BreedUIModel>) {
        breeds.clear()
        breeds.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BreedHolder {
        val inflater = LayoutInflater.from(parent.context)
        return BreedHolder(inflater.inflate(R.layout.breed_item, parent, false))
    }

    override fun getItemCount(): Int = breeds.size

    override fun onBindViewHolder(holder: BreedHolder, position: Int) {
        holder.bind(breeds[position], clickListener)
    }

    class BreedHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(model: BreedUIModel, clickListener: ClickListener) {
            itemView.breed_name.text = model.name
            itemView.setOnClickListener {
                clickListener.onItemClicked(model.referenceImageId)
            }
        }

    }

}
