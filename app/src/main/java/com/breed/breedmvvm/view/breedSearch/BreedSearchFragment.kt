package com.breed.breedmvvm.view.breedSearch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.breed.breedmvvm.R
import com.breed.breedmvvm.view.breedImage.BreedImageFragment
import com.breed.breedmvvm.view.extension.observe
import com.breed.core.ui.BaseFragment
import com.breed.presentation.breedSearch.BreedSearchViewModel
import com.breed.presentation.model.BreedUIModel
import kotlinx.android.synthetic.main.fragment_search_breeds.*

class BreedSearchFragment : BaseFragment<BreedSearchViewModel>(), ClickListener {

    private var breedListAdapter: BreedListAdapter? = null

    override fun onItemClicked(breedId: String) {
        viewModel.inputs.onBreedSelected(breedId)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_search_breeds, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonSearchBreed.isEnabled = false

        searchEditText.doAfterTextChanged {
            buttonSearchBreed.isEnabled = !searchEditText.text.isNullOrEmpty()
        }

        buttonSearchBreed.setOnClickListener {
            fetchBreeds()
        }

        setupRecyclerView()
    }

    private fun fetchBreeds() {
        if (!searchEditText.text.isNullOrEmpty()) {
            viewModel.inputs.getBreedsByName(searchEditText.text.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        observe(viewModel.searchBreedListObservable(), ::addBreedsToAdapter)
    }

    private fun setupRecyclerView() {
        breedListAdapter = BreedListAdapter(this)
        breedsRecycler.adapter = breedListAdapter
        val linearLayoutManager = LinearLayoutManager(context)
        breedsRecycler.layoutManager = linearLayoutManager
        swipeContainer.setOnRefreshListener {
            fetchBreeds()
            swipeContainer.isRefreshing = false
        }
    }

    override val viewModel: BreedSearchViewModel by lazy {
        ViewModelProviders.of(this, vmFactory).get(BreedSearchViewModel::class.java).apply {
            observe(errorResource(), ::showErrorToast)
            observe(loading(), ::showLoading)
            observe(searchBreedListObservable(), ::addBreedsToAdapter)
            observe(breedSelectedObservable(), ::navigateToDetailFragment)
        }
    }


    private fun navigateToDetailFragment(breedId: String?) {
        val args = Bundle().apply {
            putString(BreedImageFragment.BREED_IMAGE_FRAGMENT_TYPE_ARG, breedId)
        }
        findNavController().navigate(R.id.goToImageBreedFragment, args)
    }

    private fun addBreedsToAdapter(list: List<BreedUIModel>?) {
        list?.let { itemList ->
            breedListAdapter?.let { adapter ->
                adapter.setData(itemList)
                breedsRecycler.adapter = adapter
            }
        }
    }

    private fun showLoading(isLoading: Boolean?) {
        isLoading?.let {
            progressBar.visibility = if (isLoading) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }
}