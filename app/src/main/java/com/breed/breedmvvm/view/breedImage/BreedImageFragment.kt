package com.breed.breedmvvm.view.breedImage

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.breed.breedmvvm.R
import com.breed.breedmvvm.view.extension.observe
import com.breed.core.ui.BaseFragment
import com.breed.presentation.breedImage.BreedImageViewModel
import com.breed.presentation.model.BreedImageUIModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fragment_breed_image.*


class BreedImageFragment : BaseFragment<BreedImageViewModel>() {

    lateinit var breedId: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_breed_image, container, false)!!

    override fun onResume() {
        super.onResume()
        getBundledContents()
        viewModel.inputs.getBreedsImage(breedId)
    }

    override val viewModel: BreedImageViewModel by lazy {
        ViewModelProviders.of(this, vmFactory).get(BreedImageViewModel::class.java).apply {
            observe(errorResource(), ::showErrorToast)
            observe(loading(), ::showLoading)
            observe(breedImageObservable(), ::placeImage)
        }
    }

    @SuppressLint("CheckResult")
    private fun placeImage(breedImageUIModel: BreedImageUIModel?) {
        if (breedImageUIModel?.url != null) {
            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.drawable.ic_place_holder)
            requestOptions.error(R.drawable.ic_image_not_available)
            Glide
                .with(this)
                .setDefaultRequestOptions(requestOptions)
                .load(breedImageUIModel.url)
                .into(breedImageView)
        }
    }

    private fun showLoading(isLoading: Boolean?) {
        isLoading?.let {
            breedImageprogressBar.visibility = if (isLoading) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }

    private fun getBundledContents() {
        arguments?.let { bundle ->
            breedId = bundle.getString(BREED_IMAGE_FRAGMENT_TYPE_ARG)
                ?: throw IllegalStateException("BreedId is required")

        }
    }

    companion object {
        const val BREED_IMAGE_FRAGMENT_TYPE_ARG = "breedId"
    }
}