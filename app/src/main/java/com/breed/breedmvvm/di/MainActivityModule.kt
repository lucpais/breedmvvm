package com.breed.breedmvvm.di

import com.breed.breedmvvm.view.MainActivity
import com.breed.breedmvvm.view.breedImage.BreedImageFragment
import com.breed.breedmvvm.view.breedSearch.BreedSearchFragment
import com.breed.core.presentation.ViewModelProviderFactory
import com.breed.presentation.breedImage.BreedImageViewModel
import com.breed.presentation.breedSearch.BreedSearchViewModel
import com.breed.presentation.main.MainViewModel
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class MainActivityModule {
    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindSearchBreedFragment(): BreedSearchFragment

    @ContributesAndroidInjector
    internal abstract fun bindBreedImageFragment(): BreedImageFragment

    @Module
    companion object {
        @Provides
        @JvmStatic
        internal fun provideMainViewModelFactory(mainViewModel: MainViewModel): ViewModelProviderFactory<MainViewModel> {
            return ViewModelProviderFactory(mainViewModel)
        }

        @Provides
        @JvmStatic
        internal fun provideBreedSearchViewModel(breedSearchViewModel: BreedSearchViewModel): ViewModelProviderFactory<BreedSearchViewModel> {
            return ViewModelProviderFactory(breedSearchViewModel)
        }

        @Provides
        @JvmStatic
        internal fun provideBreedImageViewModelFactory(breedImageViewModel: BreedImageViewModel): ViewModelProviderFactory<BreedImageViewModel> {
            return ViewModelProviderFactory(breedImageViewModel)
        }

    }
}