package com.breed.breedmvvm.di

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.breed.core.presentation.AppSchedulerProvider
import com.breed.core.presentation.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class AppModule {

    @Provides
    fun provideContext(app: Application): Context = app.applicationContext

    @Provides
    fun provideResources(app: Application): Resources = app.resources

    @Provides
    @Singleton
    fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()

}
