package com.breed.breedmvvm.di

import com.breed.breedmvvm.BreedsApp
import com.breed.data.di.BreedApiModule
import com.breed.data.di.NetworkModule
import com.breed.domain.di.RepositoryModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        BreedApiModule::class,
        NetworkModule::class,
        RepositoryModule::class,
        MainActivityModule::class]
)
@Singleton
interface AppComponent : AndroidInjector<BreedsApp>
