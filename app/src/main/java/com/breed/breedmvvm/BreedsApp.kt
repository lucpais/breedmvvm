package com.breed.breedmvvm

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import com.breed.breedmvvm.di.DaggerAppComponent

open class BreedsApp : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerAppComponent.builder()
            .build()
}
